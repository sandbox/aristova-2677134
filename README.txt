Module Commerce Invite Discount allows authenticated users to invite users 
and gives discount when invited user makes an order and when order's status is 
changed to 'completed'.


To install this module, just download it and copy into your site's 'modules'
folder. Once copied, go to 'admin/build/modules' page and enable Commerce Invite
 Discount module. You have to install it's dependency - Commerce Discount.

Now go to module's settings page
'admin/commerce/config/commerce_invite_discount' and set exact discount percent
you want to give to inviter and configure other settings, if needed. Afterwards
go to 'admin/structure/block' page and place block with a form to add users to
some region of your theme). 

Discount is being given after an order was created by invited user, and it was
'Completed (completed)', i.e. received that status. Or you can choose to give a 
discount for simple user registration without any orders. User can see his discount
only in his shopping cart, but not in other places of the site.

Sum spent by invited user is being saved on inviter user's balance. You can set
minimal sum which should be spent by invited user to acquire discount. 
If discount-giving order changed status from 'completed' to other status (for
example, if invited user returned goods), sum of this order will be substracted
from inviter user's balance. And if sum on balance will be less than you've set
as minimal, the discount will be canceled.
