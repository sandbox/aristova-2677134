<?php

/**
 * @file
 * Admin page callback file for the Commerce Invite Discount module.
 */
function commerce_invite_discount_float_value_validate($element, &$form_state) {
    $value = $element ['#value'];
    if (($value < 0) && (!is_int($value)) && (!is_float($value))) {
        form_error($element, t('%name must be a positive integer or a positive float.', array('%name' => $element ['#title'])));
    }
}

/**
 * Configuration form.
 */
function commerce_invite_discount_admin() {
    $form = array();

    $form['commerce_invite_discount_registration_percent'] = array(
      '#type' => 'textfield',
      '#title' => t('Discount percent you want to give to an inviter in REGISTRATION'),
      '#default_value' => round(variable_get('commerce_invite_discount_registration_percent'), 2),
      '#size' => 10,
    );

    $form['commerce_invite_discount_percent'] = array(
      '#type' => 'textfield',
      '#title' => t('Discount percent you want to give to an inviter'),
      '#default_value' => round(variable_get('commerce_invite_discount_percent'), 2),
      '#element_validate' => array('element_validate_integer'),
      '#size' => 10,
    );
    $form['commerce_invite_discount_minimal_sum_spend_by_inviter_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimal sum spent by INVITER user to give him a discount'),
      '#default_value' => round(variable_get('commerce_invite_discount_minimal_sum_spend_by_inviter_user'), 2),
      '#size' => 10,
      '#description' => t("It may be reasonable to requre inviter to make some order before inviting anyone. If you don't want to require it, leave this field blank."),
      '#element_validate' => array('commerce_invite_discount_float_value_validate'),
    );
    $form['commerce_invite_discount_minimal_sum_spend_by_invited_user'] = array(
      '#type' => 'textfield',
      '#title' => t('Minimal sum spend by INVITED user to give a discount to his inviter'),
      '#default_value' => round(variable_get('commerce_invite_discount_minimal_sum_spend_by_invited_user'), 2),
      '#size' => 10,
      '#description' => t("To prevent users from inviting fake persons, it's reasonable to give a discount only after invited person have bought something. If you don't want to set such limit, leave this field blank."),
      '#element_validate' => array('commerce_invite_discount_float_value_validate'),
    );
    
    $form['commerce_invite_discount_max_discount'] = array(
      '#type' => 'textfield',
      '#size' => 10,
      '#required' => TRUE,
      '#element_validate' => array('element_validate_integer_positive'),
      '#title' => t("Maximal discount for user"),
      '#default_value' => variable_get('commerce_invite_discount_max_discount'),
      '#description' => t("Maximal discount which user can receive."),
    );
    
    $form['batch_link'] = array(
      '#type' => 'link',
      '#title' => t('update discount in database'),
      '#prefix' => t('<strong>Note:</strong> Click&nbsp;'),
      '#suffix' => t('&nbsp;link after save configuration for execution of updating discounts.'),
      '#href' => 'admin/commerce/config/commerce_invite_discount/update_discounts',
    );
  
  return system_settings_form($form);
}

/**
 * Implements validation handler for commerce_invite_discount_admin_form().
 *
 * Check if mandatory fields were filled.
 * There are fields that are being revealed only in case certain options were
 * selected. Mail titles and bodies are among them, and we don't want to send
 * messages without titles or bodies.
 */
function commerce_invite_discount_admin_validate($form, &$form_state) {
  $discount_sum = $form_state['values']['commerce_invite_discount_percent'] + $form_state['values']['commerce_invite_discount_registration_percent'];
   
  if ($discount_sum > 100 || $discount_sum < 0) {
    $error_elements = array('commerce_invite_discount_registration_percent', 'commerce_invite_discount_percent');
    foreach ($error_elements as $element) {
      form_set_error($element);
    }
    drupal_set_message(t('The discount percent must be between 0 % to 100 %'), 'error');
  }
  if (empty($form_state['values']['commerce_invite_discount_minimal_sum_spend_by_inviter_user'])) {
    variable_set('commerce_invite_discount_minimal_sum_spend_by_inviter_user', 0);
  }
  if (empty($form_state['values']['commerce_invite_discount_minimal_sum_spend_by_invited_user'])) {
    variable_set('commerce_invite_discount_minimal_sum_spend_by_invited_user', 0);
  }
  if (empty($form_state['values']['commerce_invite_discount_registration_percent']) && empty($form_state['values']['commerce_invite_discount_percent'])) {
    $error_elements = array('commerce_invite_discount_registration_percent', 'commerce_invite_discount_percent');
    foreach ($error_elements as $element) {
      form_set_error($element);
    }
    drupal_set_message(t('You should fill one of these fields'), 'error');
    
   }
}

