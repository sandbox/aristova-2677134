<?php

/**
 * @file
 * Install file for the module.
 */

/**
 * Implements hook_install().
 */
function commerce_invite_discount_install() {
  commerce_invite_discount_batch($sandbox);
}

/**
 * Implements hook_schema().
 */
function commerce_invite_discount_schema() {
  $schema['commerce_invite_discount_inviter_users'] = array(
    'description' => "This table contains UIDs of inviter users and a sums which were spent by users they've invited",
    'fields' => array(

      'inviter_uid' => array(
        'type' => 'int',
        'not null' => TRUE,
      ),

      'sum_spent_by_invited' => array(
        'type' => 'float',
        'not null' => TRUE,
        'default' => 0.00,
      ),

      'discount_percent'  => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),

      'discount_percent_registration'  => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),

      'sum_spent_by_inviter' => array(
        'type' => 'float',
        'not null' => TRUE,
        'default' => 0.00,
      ),

      'code' => array(
        'type' => 'text',
        'not null' => TRUE,
      ),
    ),

    'unique keys' => array(
      'inviter_uid' => array('inviter_uid'),
    ),
  );

  // Table contains matches between UIDs of inviter and a user, invited by him.
  // So there may be many entries with the same inviter UID.
  // But there may be only one entry with invited user per column.
  $schema['commerce_invite_discount_invited_users'] = array(
    'description' => 'This table contains UIDs of inviters and invited users',
    'fields' => array(
      'inviter_uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'invited_uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
    ),
    'indexes' => array(
      'inviter_uid'  => array('inviter_uid'),
    ),
    'unique keys' => array(
      'invited_uid' => array('invited_uid'),
    ),
  );

  return $schema;
}

/**
 * Implements hook_uninstall().
 */
function commerce_invite_discount_uninstall() {
  // Get global variable array.
  global $conf;
  // Delete all persistent variables created by this module.
  foreach (array_keys($conf) as $key) {
    // Find variables that have the module prefix.
    if (strpos($key, 'commerce_invite_discount_') == 0) {
      variable_del($key);
    }
  }
}
